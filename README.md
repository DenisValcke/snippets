# Visual Studio Code Snippets

## Dependencies
_To use these snippets you will need to install the [Project Snippets](https://marketplace.visualstudio.com/items?itemName=rebornix.project-snippets)_

## Installation
### Create a `snippets`-directory
Move to the `.vscode` directory and create a directory where the extension can find your snippets:
```
cd ./vscode
mkdir snippets
cd snippets
```

### Clone this repo
Now clone this repo in the snippets folder to start using the snippets.
```
git@bitbucket.org:DenisValcke/snippets.git
```

## Contribute
To contribute to the snippets please commit your changes to a seperate branch, open a PR and tag two of your teammates to double check if everything is in order. After the PR is merged everyone can go ahead and use your contributions.